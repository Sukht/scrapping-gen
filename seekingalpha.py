import requests
from bs4 import BeautifulSoup
import pandas as pd
import time
from random import randint
from selenium import webdriver
from selenium.webdriver.firefox.options import Options

options = Options()
options.add_argument('--headless')
browser = webdriver.Firefox(options=options)
session = requests.session()


# Function that extracts info for info.csv from given soup
def get_info(soup):
    lines = []

    # Truncate data to only the relavent section
    for div in soup.find_all('div', {'class': 'sa-art article-width'}):
        for p in div.find_all('p', {'class': 'p p1'}):
            if p.text == 'Operator':
                break
            lines.append(p.text)

    # Break up date information
    temp = lines[0].split(' ')
    date = temp[-7::]
    del(date[3])
    date[1] = date[1].split(',')[0]

    # Extractact exchange and symbol
    infos = 0
    for i in range(len(temp)):
        if '(' in temp[i] and ')' in temp[i] and ':' in temp[i]:
            infos = i
            break

    temp2 = temp[infos].replace('(', '').replace(')', '').split(':')
    sym = temp2[1]
    ext = temp2[0]

    # Extract company name
    name_t = temp[0:infos]
    name = ' '.join(name_t)

    # Extract list of executives and analysts
    desc = lines[0]
    ana = 0
    for i in range(1, len(lines)):
        if lines[i] == 'Analysts':
            ana = i
            break

    if ana != 0 and ana != 1 and ana != 2:
        execs = lines[2: ana]
        anas = lines[ana+1::]
    elif ana == 1 or ana == 2:
        execs = []
        anas = lines[ana+1::]
    else:
        execs = lines[2::]
        anas = []

    speakers = []
    for execo in execs:
        tempn = execo.split(' - ')
        speakers.append((tempn[0], tempn[1], 'E'))

    for anay in anas:
        tempn = anay.split(' - ')
        speakers.append((tempn[0], tempn[1], 'A'))

    # create a list with the relavent information per speaker as its enteries
    # the return a list of these lists
    info_tot = []
    for speaker in speakers:
        infob = [name, ext, sym, desc]
        for ite in date:
            infob.append(ite)
        for ite in speaker:
            infob.append(ite)
        info_tot.append(infob)
    return info_tot


# Function that extracts info for presentation.csv from given URL, information from get_info() is also used to fill in
# certain components
def get_presentation(soup, info):
    # build a list of needed information present in info
    pre = [info[0][0], info[0][2], info[0][4], info[0][5], info[0][6]]

    # Truncate data to relevant components
    for div in soup.find_all('div', {'class': 'sa-art article-width'}):
        start = 0
        end = 0
        liny = div.find_all('p', {'class': 'p'})
        for i in range(0, len(liny)):
            if liny[i].text == 'Operator':
                start = i
                break
        for i in range(start, len(liny)):
            try:
                a = liny[i]["id"]
                end = i
                break
            except Exception as e:
                a = e

        liny2 = liny[start: end]

        # Build speaches per session
        current = ''
        sent = ''
        speaches = []
        for line in liny2:
            if line.find('strong'):
                if current == '':
                    current = line.find('strong').text
                else:
                    speach = pre + [current, sent, len(sent.replace('[Operator Instructions]', '').split(' ')),
                                    len(sent.split('.'))]
                    speaches.append(speach)
                    current = line.find('strong').text
                    sent = ''
            else:
                sent = sent + line.text

        # Combine newly extracted data and needed information from info
        speach = pre + [current, sent, len(sent.replace('[Operator Instructions]', '').split(' ')),
                        len(sent.split('.'))]
        speaches.append(speach)
        return speaches


# Function that extracts info for qa.csv from given URL, information from get_info() is also used to fill in
# certain components
def get_qa(soup, info):
    # build a list of needed information present in info
    pre = [info[0][0], info[0][2], info[0][4], info[0][5], info[0][6]]

    emps = {'Operator': 'O'}

    for ent in info:
        emps[ent[-3]] = ent[-1]

    for div in soup.find_all('div', {'class': 'sa-art article-width'}):
        start = 0
        end = 0
        liny = div.find_all('p', {'class': 'p'})
        for i in range(0, len(liny)):
            if liny[i].text == 'Operator':
                start = i
                break
        for i in range(start, len(liny)):
            try:
                a = liny[i]["id"]
                if a == 'question-answer-session':
                    start = i
                elif a == 'comment-session':
                    end = i
                    break

            except Exception as e:
                a = e

        if end == 0:
            liny2 = liny[start+1::]
        else:
            liny2 = liny[start+1:end]

        ids = 0
        speaches = []
        speaker = ''
        role = ''
        nid = ''
        for line in liny2:
            if line.find('strong'):
                speaker = line.find('strong').text.strip()
                role = emps[line.find('strong').text.strip()]
                if emps[line.find('strong').text.strip()] == 'O':
                    ids += 1
                    nid = str(ids)
            else:
                speach = pre + [speaker, role, nid, line.text, len(line.text.replace('[Operator Instructions]', '')
                                                                   .split(' ')), len(line.text.split('.'))]
                speaches.append(speach)
        return speaches


# Function that writes info.csv
def info_csv(info):
    # Break info data into separate lists
    name = []
    exchange = []
    symbol = []
    description = []
    mon = []
    day = []
    year = []
    timer = []
    apm = []
    timezone = []
    name_speaker = []
    title = []
    role = []

    for ent in info:
        name.append(ent[0])
        exchange.append(ent[1])
        symbol.append(ent[2])
        description.append(ent[3])
        mon.append(ent[4])
        day.append(ent[5])
        year.append(ent[6])
        timer.append(ent[7])
        apm.append(ent[8])
        timezone.append(ent[9])
        name_speaker.append(ent[10])
        title.append(ent[11])
        role.append(ent[12])

    # convert lists into a dataframe
    df = pd.DataFrame({'Name': name, 'Exchange': exchange, 'Symbol': symbol, 'Description': description, 'Mon': mon,
                       'Day': day, 'Year': year, 'Time': timer, 'APM': apm, 'TimeZone': timezone,
                       'Name_Speaker': name_speaker, 'Title': title, 'Role': role})
    header = (['Name', 'Exchange', 'Symbol', 'Description', 'Mon', 'Day', 'Year', 'Time', 'APM', 'TimeZone', 'Name',
               'Title/Institution', 'Role'])
    # Write data frame to csv
    df.to_csv('info.csv', header=header, index=False)


# Function that writes presentation.csv
def presentation_csv(pres):
    # Break pres data into separate lists
    name = []
    symbol = []
    mon = []
    day = []
    year = []
    speaker = []
    content = []
    words = []
    sentences = []

    for ent in pres:
        name.append(ent[0])
        symbol.append(ent[1])
        mon.append(ent[2])
        day.append(ent[3])
        year.append(ent[4])
        speaker.append(ent[5])
        content.append(ent[6])
        words.append(ent[7])
        sentences.append(ent[8])

    # convert lists into a dataframe
    df = pd.DataFrame({'Name': name, 'Symbol': symbol, 'Mon': mon, 'Day': day, 'Year': year, 'Speaker': speaker,
                       'Content': content, 'words': words, 'sentences': sentences})
    header = (['Name', 'Symbol', 'Mon', 'Day', 'Year', 'Speaker', 'Content', '\#words', '\#sentences'])
    # Write data frame to csv
    df.to_csv('presentation.csv', header=header, index=False)


# Function that writes qa.csv
def qa_csv(qa):
    # Break qa data into separate lists
    name = []
    symbol = []
    mon = []
    day = []
    year = []
    speaker = []
    role = []
    themeid = []
    content = []
    words = []
    sentences = []

    for ent in qa:
        name.append(ent[0])
        symbol.append(ent[1])
        mon.append(ent[2])
        day.append(ent[3])
        year.append(ent[4])
        speaker.append(ent[5])
        role.append(ent[6])
        themeid.append(ent[7])
        content.append(ent[8])
        words.append(ent[9])
        sentences.append(ent[10])

    # convert lists into a dataframe
    df = pd.DataFrame({'Name': name, 'Symbol': symbol, 'Mon': mon, 'Day': day, 'Year': year, 'Speaker': speaker,
                       'Role': role, 'ThemeID': themeid, 'Content': content, 'words': words, 'sentences': sentences})
    header = (['Name', 'Symbol', 'Mon', 'Day', 'Year', 'Speaker', 'role', 'ThemeID', 'Content', '\#words',
               '\#sentences'])
    # Write data frame to csv
    df.to_csv('qa.csv', header=header, index=False)


# Function that writes presentation_no_content.csv
def presentation_no_csv(pres):
    # Break pres data into separate lists
    name = []
    symbol = []
    mon = []
    day = []
    year = []
    speaker = []
    words = []
    sentences = []

    for ent in pres:
        name.append(ent[0])
        symbol.append(ent[1])
        mon.append(ent[2])
        day.append(ent[3])
        year.append(ent[4])
        speaker.append(ent[5])
        words.append(ent[7])
        sentences.append(ent[8])

    # convert lists into a dataframe
    df = pd.DataFrame({'Name': name, 'Symbol': symbol, 'Mon': mon, 'Day': day, 'Year': year, 'Speaker': speaker,
                       'words': words, 'sentences': sentences})
    header = (['Name', 'Symbol', 'Mon', 'Day', 'Year', 'Speaker', '\#words', '\#sentences'])
    # Write data frame to csv
    df.to_csv('presentation_no_content.csv', header=header, index=False)


# Function that writes qa_no_content.csv
def qa_no_csv(qa):
    # Break qa data into separate lists
    name = []
    symbol = []
    mon = []
    day = []
    year = []
    speaker = []
    role = []
    themeid = []
    words = []
    sentences = []

    for ent in qa:
        name.append(ent[0])
        symbol.append(ent[1])
        mon.append(ent[2])
        day.append(ent[3])
        year.append(ent[4])
        speaker.append(ent[5])
        role.append(ent[6])
        themeid.append(ent[7])
        words.append(ent[9])
        sentences.append(ent[10])

    # convert lists into a dataframe
    df = pd.DataFrame({'Name': name, 'Symbol': symbol, 'Mon': mon, 'Day': day, 'Year': year, 'Speaker': speaker,
                       'Role': role, 'ThemeID': themeid, 'words': words, 'sentences': sentences})
    header = (['Name', 'Symbol', 'Mon', 'Day', 'Year', 'Speaker', 'role', 'ThemeID', '\#words', '\#sentences'])
    # Write data frame to csv
    df.to_csv('qa_no_content.csv', header=header, index=False)


# Fetch soup for given URL
def get_soup(file_url):
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0'}
    # headers = {}
    re = session.get(file_url, headers=headers)
    soup = BeautifulSoup(re.text, 'html.parser')
    return soup


def get_soup2(file_url):
    browser.get(file_url)
    soup = BeautifulSoup(browser.page_source, 'lxml')
    # browser.quit()
    return soup

# url = 'https://seekingalpha.com/article/4212287-alcoa-corporations-aa-ceo-roy-harvey-q3-2018-results-earnings-call-' \
#       'transcript?part=single'
#
# soupy = get_soup(url)
# infosy = get_info(soupy)
# info_csv(infosy)
# presentation_csv(get_presentation(soupy, infosy))
# presentation_no_csv(get_presentation(soupy, infosy))
# qa_no_csv(get_qa(soupy, infosy))


def graby(co):
    url = 'https://seekingalpha.com/earnings/earnings-call-transcripts/{}'.format(str(co))
    soupy = get_soup2(url)
    lisy = soupy.find_all('li', {'class': 'list-group-item article'})
    return lisy


def get_list():
    global browser
    file = open('art_list.txt', 'w+')
    for i in range(4228, 5252):
        lisor = []
        while not lisor:
            lisor = graby(i)
            if not lisor:
                browser.quit()
                time.sleep(120)
                browser = webdriver.Firefox(options=options)

        for li in lisor:
            for a in li.find_all('a', {'class': 'dashboard-article-link'}):
                file.write(li['data-id'] + '; ' + a['href'] + '\n')

        print(i)
    file.close()


get_list()
