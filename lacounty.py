from PyPDF2 import PdfFileReader
import requests

headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0'}
file_url = 'https://wdacs.lacounty.gov/Centers/Senior Center Directory 4-18-18.pdf'
re = requests.get(file_url, headers=headers, verify=False)

# pdf = PdfFileReader(re.content)
# info = pdf.getDocumentInfo()
# number_of_pages = pdf.getNumPages()
#
# print(info)
with open('temp.pdf', 'wb') as f:
    f.write(re.content)

with open('temp.pdf', 'rb') as f:
    pdf = PdfFileReader(f)
    info = pdf.getDocumentInfo()
    number_of_pages = pdf.getNumPages()
    entries = []
    for p in range(number_of_pages):
        page = pdf.getPage(p)
        temp = page.extractText().split('\n')
        newE = []
        for i in range(len(temp)):
            isf = False
            for c in temp[i]:
                if c.isdigit():
                    if '.' in temp[i]:
                        isf = True

                elif c.isalpha():
                    isf = False
                    break
            if isf:
                newE.append(i)
                # print(temp[i])

        for i in range(len(newE)-1):
            entries.append(temp[newE[i]+5:newE[i+1]])

for entry in entries:
    print(entry)
