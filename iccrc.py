import requests
import os.path
from os import listdir
from io import open as iopen
from os import makedirs
from stem import Signal
from stem.control import Controller

session = requests.session()
session.proxies = {}
# Add Tor support
session.proxies['http'] = 'socks5h://localhost:9050'
session.proxies['https'] = 'socks5h://localhost:9050'


def rotate():
    with Controller.from_port(port=9051) as controller:
        controller.authenticate()
        controller.signal(Signal.NEWNYM)


# get data
empty = False
current = 0
infos = []
# while not empty:
while current < 200:
    try:
        params = {'form_fields': 'lang=en&search_last_name=&search_first_name=&search_iccrc_number=&search_membership_'
                                 'status=1&search_company_name=&search_agent=&search_country=&search_province=&search_'
                                 'postal_code=&search_city=&search_street=', 'query': '', 'letter': '',
                  'start': current}
        url = 'https://secure.iccrc-crcic.ca/search/do/lang/en'
        data = session.post(url, params=params)

        results = data.json()['results']

        for res in results:
            infos.append((res['step1_surname'], res['step1_given_name'], res['form_id']))

        current += 25

    except Exception as e:
        empty = True
        print(e)


dir_path = os.path.dirname(os.path.realpath(__file__))
save_path = os.path.join(dir_path, "iccrc")
if not os.path.isdir(save_path):
        makedirs(save_path)
files = listdir(save_path)

url2 = 'https://secure.iccrc-crcic.ca/default/search/generate-email-image?nocache=203&lang=en&form_id=960&token='
count = 0
for item in infos:
    param2 = {'form_id': item[2]}
    file_name = str(item[2]) + '.png'

    if file_name not in files:
        re = session.post(url2, params=param2)
        complete_name = os.path.join(save_path, file_name)
        with iopen(complete_name, 'wb') as file:
            file.write(re.content)
            file.close()
        count += 1
        if count % 25 == 0:
            print('rotate')
            rotate()

