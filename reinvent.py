from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.options import Options
from time import sleep
from bs4 import BeautifulSoup
import io

# Venetian, Encore, Aria, MGM, Mirage, BELLAGIO, VDARA
VENUE_CODES = [22188, 728, 22191, 22190, 22583, 22584, 24372]
# Sunday, Monday, Tuesday, Wednesday, Thursday, Friday
DAY_ID = [130, 170, 31, 110, 111, 112]

CHROME_DRIVER = './chromedriver'

# Initialize headless chrome
chrome_options = Options()
chrome_options.add_argument("--headless")
content_to_parse = ''

driver = webdriver.Chrome(options=chrome_options, executable_path=CHROME_DRIVER)


# Getting content by day, instead of the entire set, because sometimes the
# Get More Results link stops working on the full list. Haven't had issues
# looking at the lists day by day.
clicko = False
# Break down to venues
for venue in VENUE_CODES:
    print("Getting Content for Venue Code: " + str(venue))
    # Break down by types
    for day in DAY_ID:
        driver.get("https://www.portal.reinvent.awsevents.com/connect/search.ww#loadSearch-searchPhrase=&searchType="
                   "session&tc=0&sortBy=daytime&dayID={}&p=&i(728)={}".format(str(day), str(venue)))
        sleep(3)
        more_results = True
        # Click through all of the session results pages for a specific venue and day.
        # The goal is to get the full list for a day loaded.

        # Remove accept cookies banner
        if not clicko:
            try:
                driver.find_element_by_id('cookie-banner-accept-screen').click()
            except NoSuchElementException as e:
                clicko = True

        while more_results:
            try:
                # Find the Get More Results link and click it to load next sessions
                driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
                get_results_btn = driver.find_element_by_link_text("Get More Results")
                get_results_btn.click()
                sleep(3)
            except NoSuchElementException as e:
                more_results = False

        # Click View More to expand abstracts
        get_btns = driver.find_elements_by_link_text("View More")
        for btn in get_btns:
            btn.click()

        # Once all sessions for the day have been loaded by the headless browser,
        # append to a variable for use in BS.
        content_to_parse = content_to_parse + driver.page_source

driver.close()

# Start the process of grabbing out relevant session information and writing to a file
# soup = BeautifulSoup(content_to_parse, "html5lib")
soup = BeautifulSoup(content_to_parse, "html.parser")

# In some event titles, there are audio options available inside of an 'i' tag
# Strip out all 'i' tags to make this easier on BS
# Hopefully there is no other italicized text that I'm removing
for i in soup.find_all('i'):
    i.extract()

# Grab all of the sessionRows from the final set of HTML and work only with that
sessions = soup.find_all("div", class_="sessionRow")

# Open a blank text file to write sessions to
file = io.open("sessions.txt", "w", encoding='utf-8')

# Create a header row for the file. Note the PIPE (|) DELIMITER.
file.write("Session Number|Title|Abstract\n")

ses = []

# For each session, pull out the relevant fields and write them to the sessions.txt file.
for session in sessions:
    session_soup = BeautifulSoup(str(session), "html.parser")
    session_number = session_soup.find("span", class_="abbreviation")
    session_number = session_number.string.replace(" - ", "")

    if session_number not in ses:
        ses.append(session_number)
        session_title = session_soup.find("span", class_="title")
        session_title = session_title.string.encode('utf-8').rstrip()
        session_title = session_title.decode('utf-8')

        session_abstract = session_soup.find("span", class_="abstract")
        session_abstract = session_abstract.text.replace('\n', ' ').encode('utf-8').rstrip()
        session_abstract = session_abstract.decode('utf-8')

        # Remove View Less from end of abstracts
        if session_abstract[-9::] == 'View Less':
            session_abstract = session_abstract[0:-9]

        write_contents = str(session_number) + "|" + session_title + "|" + session_abstract
        file.write(write_contents.strip() + "\n")

file.close()
