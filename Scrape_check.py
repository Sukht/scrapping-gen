import requests as re
from bs4 import BeautifulSoup as bs
import pandas as pd
import string

# get data
# data = re.get('https://www.cfainstitute.org/community/membership/directory/pages/results.aspx?FirstName=a&LastName=&City=&State=&Province=&Country=&Employer=&Society=')
data = re.get('https://secure.iccrc-crcic.ca/search-new/EN')
# load data to bs4
soup = bs(data.text, 'html.parser')

print(soup.prettify())

# for div in soup.find_all('div', {'class': 'div-table__cell vis-categories__content'}):
#     print(div.text)
