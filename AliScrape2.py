import requests as re
from bs4 import BeautifulSoup as bs
import csv


session = re.session()
session.proxies = {}
# Add Tor support
# session.proxies['http'] = 'socks5h://localhost:9050'
# session.proxies['https'] = 'socks5h://localhost:9050'


def storescrap(storeid):
    items = []
    i = 0
    pages = True
    while pages:
        i += 1
        page = str(i)
        # get data
        data = session.get('https://www.aliexpress.com/store/{}/search/{}.html?'.format(storeid, page))
        # load data to bs4
        soup = bs(data.text, 'html.parser')
        
        # Find items listed
        lis = soup.find_all('li', {'class': 'item'})
        lis2 = []
        # Clean up found items, keeping only actual listed items and removing propped items
        for li in lis:
            if li.has_attr('class'):
                if len(li['class']) == 1:
                    lis2.append(li)
        # Get info of found items, if items on page
        if lis2:
            for li in lis2:
                temp1 = li.find('div', {'class': 'detail'}).find('a', href=True, title=True)
                temp2 = li.find('div', {'class': 'cost'})
                if temp2:
                    temp2 = temp2.find('b').text.split('$')[1]
                else:
                    temp2 = li.find('div', {'class': 'price'}).find('span').text.split('$')[1]

                temp3 = li.find('span', {'class': "money"})
                if temp3:
                    temp3 = temp3.text.strip().split('$')[1]
                else:
                    if temp2:
                        temp3 = temp2

                if not temp2:
                    temp2 = temp3
                items.append((temp1['title'], 'https:'+temp1['href'], temp2, temp3))
                
        # If no items found on page break loop
        else:
            print('empty page', i)
            break

    # Write found data to CSV
    with open('{}.csv'.format(storeid), 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=';', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for item in items:
            writer.writerow([item[0], item[1], item[2], item[3]])


storeid1 = '1836844'

storescrap(storeid1)
