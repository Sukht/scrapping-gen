import requests as re
from bs4 import BeautifulSoup
import pandas as pd
import string

companies = []
letters = string.ascii_lowercase

# Scrape companies starting with each letter
for char in letters:
    try:
        # get data
        data = re.get('https://www.valveworldexpo.com/vis/v1/en/directory/{}?oid=8560&lang=2'.format(char))

        # load data to bs4
        soup = BeautifulSoup(data.text, 'html.parser')

        # Get list of companies and their names
        shops = []
        for div in soup.find_all('div', {'class': 'exh-table-item'}):
            for a in div.find_all('a', {'class': 'flush'}, href=True):
                shops.append((str(a.text), 'https://www.valveworldexpo.com'+str(a['href'])))

        # Get company details
        for shop in shops:
            # Grab per shop
            data = re.get(shop[1])
            soup = BeautifulSoup(data.text, 'html.parser')

            # Address + country
            address = ''
            country = ''
            for div in soup.find_all('div', {'itemprop': 'address'}):
                address1 = div.find('span', {'itemprop': 'streetAddress'})
                if address1:
                    address += address1.text
                address2 = div.find('span', {'itemprop': 'postalCode'})
                if address2:
                    address += ', ' + address2.text
                address3 = div.find('span', {'itemprop': 'addressLocality'})
                if address3:
                    address += ' ' + address3.text
                country = div.find('span', {'itemprop': 'addressCountry'}).text

            # Telephone
            tel1 = soup.find('span', {'itemprop': 'telephone'})
            if tel1:
                tel = tel1.text
            else:
                tel = ''

            # Fax
            fax1 = soup.find('span', {'itemprop': 'faxNumber'})
            if fax1:
                fax = fax1.text
            else:
                fax = ''

            # Email
            email1 = soup.find('a', {'itemprop': 'email'})
            if email1:
                email = email1.text
            else:
                email = ''

            # Website
            web1 = soup.find('a', {'itemprop': 'url'})
            if web1:
                web = web1.text
            else:
                web = ''

            # Location
            location = soup.find('span', {'class': 'link-fix--text'}).text

            # Product Categories
            category = ''
            for div in soup.find_all('div', {'class': 'div-table__cell vis-categories__content'}):
                if category == '':
                    category = div.text
                else:
                    category = category + ', ' + div.text

            # Create list containing details for each company
            companies.append((shop[0].replace('\n', ''), address.replace('\n', ''), country.replace('\n', ''),
                              tel.replace('\n', ''), fax.replace('\n', ''), email.replace('\n', ''),
                              web.replace('\n', '').replace('\xa0', '').strip(), location.replace('\n', ''),
                              category.replace('\n', '')))
    except Exception as e:
        print(char)
        print(e)

# Write scraped data to spreadsheet
df = pd.DataFrame(companies, columns=['Company Name', 'Address', 'Country', 'Tel', 'Fax', 'Email', 'Website',
                                      'Location', 'Product categories'])
writer = pd.ExcelWriter('valveworldexpo-final-2018.xls')
df.to_excel(writer, 'Sheet1', index=False)
writer.save()
