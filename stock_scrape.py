import requests as re
from bs4 import BeautifulSoup as bs

rate = 0


# Function to extract exchange rate from AUD to USD
def get_rate():
    global rate
    data = re.get('https://www.marketindex.com.au/australian-dollar')
    soup = bs(data.text, 'html.parser')
    div1 = soup.find('div', {'class': 'currency-inner'})
    temp = div1.text.split('\n')
    rate = float(temp[1][1::])


# Function that scrapes the ASX300 stock prices
def get_asx300():
    # Setup header, passing User-Agent and Origin
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0',
               'Origin': 'https://www.marketindex.com.au'}
    # URL from which to scrape information
    url = 'https://quoteapi.com/api/v5/symbol-lists/mjg300?appID=af5f4d73c1a54a33&averages=1&customData=1&desc=1&' \
          'fundamentals=1&liveness=delayed&pickFields=CNSPCpHLVSiYo'
    # Grab data and parse JSON
    data = re.get(url, headers=headers)
    results = data.json()['items']

    asx300 = {}

    # parse results to dictionary
    for item in results:
        if item['quote']['price']:
            key = str(item['symbol']).split('.')[0].upper()
            value = (item['desc']['name'], round(item['quote']['price']/rate, 2))
        # else:
        #     value = (item['desc']['name'], item['quote']['price'])
            asx300[key] = value

    return asx300


# Function that scrapes the S&P 500 stock prices
def get_sp500():
    get = False
    while not get:
        try:
            # Initial header for retrieving cookie
            headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0'}

            # URL from which to scrape list of companies and stock prices
            url2 = 'https://www.barchart.com/proxies/core-api/v1/quotes/get?list=stocks.markets.sp500&fields=symbol,' \
                   'symbolName,lastPrice,priceChange,percentChange,highPrice,lowPrice,volume,tradeTime,symbolCode,' \
                   'hasOptions,symbolType&orderBy=&orderDir=desc&meta=field.shortName,field.type,field.description&' \
                   'hasOptions=true&raw=1'
            # URL from which to get cookies needed for scraping access
            url = 'https://www.barchart.com'
            # Grab cookies
            data = re.get(url, headers=headers)
            dadi = dict(data.cookies)
            # Setup header for data scraping, passing User-Agent and token
            headers2 = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0',
                        'X-XSRF-TOKEN': dadi['XSRF-TOKEN']}

            # Setup parameters for grabbing all companies
            params = {
                'fields': 'symbol,symbolName,lastPrice,priceChange,percentChange,highPrice,lowPrice,volume,tradeTime,'
                          'symbolCode,hasOptions,symbolType', 'hasOptions': 'true', 'list': 'stocks.markets.sp500',
                'meta': 'field.shortName,field.type,field.description', 'orderBy': '', 'orderDir': 'desc', 'raw': 1}

            # grab data and parse JSON
            data2 = re.get(url2, headers=headers2, params=params, cookies=dadi)
            data_keep = data2.json()
            get = True

        except Exception:
            # keep looping till connection made
            get = False

    sp500 = {}

    # parse results to dictionary
    for item in data_keep['data']:
        if item['lastPrice']:
            key = item['symbol']
            value = (item['symbolName'], float(item['lastPrice'].replace(',', '')))
            sp500[key] = value

    return sp500


# Function that returns combined dictionaries
def get_dict():
    get_rate()
    return {**get_asx300(), **get_sp500()}


# Function that writes dictionary to text file
def write_dict(dicti, start=0, end=1000, f_name='output.txt'):
    ress = open(f_name, 'w')
    count = 0
    it = []
    items = dicti.items()
    for item in items:
        if count >= start:
            it.append(item)
        count += 1
        if count == end:
            break
    ss = str(dict(it))
    ress.write(ss)
    ress.close()


# print(get_asx300())
# print()
# print(get_sp500())
# print()
full_dict = get_dict()
write_dict(full_dict)
